public class Hamster{

	private String name;
	private String color;
	private int weight;
	private int age;
	
	public Hamster(String name, String color, int weight, int age){
		this.name = name;
		this.color = color;
		this.weight = weight;
		this.age = age;
	}
	
	public void setName(String newName){
		name = newName;
	}
	
	public String getName(){
		return this.name;
	}
	public String getColor(){
		return this.color;
	}
	public int getWeight(){
		return this.weight;
	}
	public int getAge(){
		return this.age;
	}
	
	public void cute(){
		System.out.println("WHAT A CUTE HAMSTER YOU HAVE THERE");
	}
	
	public void size(){
		if (weight<85){
			System.out.println("you hamster is starving");
		}else if (weight>140){
			System.out.println("you hamster is overweight");
		}else {
			System.out.println("you hamster is healthy");
		}
	}

}