import java.util.Scanner;
public class VirtualPetApp{
	public static void main(String[]args){
		
		Scanner reader= new Scanner(System.in);
		
		Hamster[] hamsterfamily= new Hamster[1];
		
		for (int i=0;i<hamsterfamily.length;i++){

			System.out.println("What is your hamster name?");
			String nameInput = reader.nextLine();
			
			System.out.println("What colors is it?");
			String colorInput = reader.nextLine();
			
			System.out.println("What is his/her weight?");
			int weightInput = Integer.parseInt(reader.nextLine());
			
			System.out.println("What is his/her age in month?");
			int ageInput = Integer.parseInt(reader.nextLine());	
			
			hamsterfamily[i]= new Hamster(nameInput, colorInput, weightInput, ageInput);
		}
		
		System.out.println("Name: " + hamsterfamily[hamsterfamily.length-1].getName());
		System.out.println("Color: " + hamsterfamily[hamsterfamily.length-1].getColor());
		System.out.println("Weight: " + hamsterfamily[hamsterfamily.length-1].getWeight());
		System.out.println("Age: " + hamsterfamily[hamsterfamily.length-1].getAge());
		
		System.out.println("What is your hamster name again?");
		hamsterfamily[hamsterfamily.length-1].setName(reader.nextLine());
		
		System.out.println("Name: " + hamsterfamily[hamsterfamily.length-1].getName());
		System.out.println("Color: " + hamsterfamily[hamsterfamily.length-1].getColor());
		System.out.println("Weight: " + hamsterfamily[hamsterfamily.length-1].getWeight());
		System.out.println("Age: " + hamsterfamily[hamsterfamily.length-1].getAge());
	}
}